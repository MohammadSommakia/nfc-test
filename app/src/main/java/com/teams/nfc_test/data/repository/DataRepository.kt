package com.teams.nfc_test.data.repository

import com.teams.nfc_test.data.local.repository.LocalRepository
import com.teams.nfc_test.data.remote.controller.Resource
import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import com.teams.nfc_test.model.Transaction
import javax.inject.Inject

class DataRepository
@Inject
constructor(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : DataSource {

    override suspend fun getExchangeRate(): Resource<ExchangeRateResponse> {
        return remoteRepository.getExchangeRate()
    }

    override suspend fun insertTransaction(transaction: Transaction) {
        return localRepository.insertTransaction(transaction)
    }

    override suspend fun getLastRecordForBalance(balanceNumber: Int): List<Transaction> {
        return localRepository.getLastRecordForBalance(balanceNumber)


    }
}

