package com.teams.nfc_test.data.local.repository

import com.teams.nfc_test.data.local.repository.dao.TransactionDao
import com.teams.nfc_test.model.Transaction
import javax.inject.Inject

class LocalRepository
@Inject constructor(
    private val transactionDao: TransactionDao
) {
     fun insertTransaction(transaction: Transaction) {
        return transactionDao.insert(transaction)
    }

    fun getLastRecordForBalance(balanceNumber: Int): List<Transaction> {

        return transactionDao.getLastRecordForBalance(balanceNumber)
    }


}