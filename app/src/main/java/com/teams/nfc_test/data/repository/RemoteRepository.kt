package com.teams.nfc_test.data.repository

import com.google.gson.Gson
import com.teams.nfc_test.data.remote.controller.ErrorManager
import com.teams.nfc_test.data.remote.controller.IRestApiManager
import com.teams.nfc_test.data.remote.controller.Resource
import com.teams.nfc_test.data.remote.controller.ServiceGenerator
import com.teams.nfc_test.data.remote.services.IAuthService
import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject


class RemoteRepository
@Inject constructor(private val serviceGenerator: ServiceGenerator, private val gson: Gson) :
    IRestApiManager {

    @Suppress("BlockingMethodInNonBlockingContext")

    override suspend fun getExchangeRate(): Resource<ExchangeRateResponse> {
        val authService = serviceGenerator.createService(IAuthService::class.java)
        try {
            val response = authService.getExchangeRate()

            return if (response.isSuccessful) {
                //Do something with response e.g show to the UI.
                Resource.Success(response.body() as ExchangeRateResponse)
            } else {
                val errorBody = gson.fromJson(
                    response.errorBody()?.string(),
                    ErrorManager::class.java
                )
                errorBody.code = response.code()
                Resource.DataError(errorBody)
            }
        } catch (e: HttpException) {
            return Resource.Exception(e.message() as String)
        } catch (e: Throwable) {
            return Resource.Exception(errorMessage = e.message as String)
        }
        catch (e: SocketTimeoutException) {
            return Resource.Exception(errorMessage = e.message as String)
        }
        catch (e: IOException)
        {
            return Resource.Exception(errorMessage = e.message as String)

        }
    }
}