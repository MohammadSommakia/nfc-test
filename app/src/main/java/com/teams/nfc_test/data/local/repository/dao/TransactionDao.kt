package com.teams.nfc_test.data.local.repository.dao

import androidx.room.*
import com.teams.nfc_test.model.Transaction

@Dao
interface TransactionDao {


    @Insert
    fun insert(transaction: Transaction)

    @Query("SELECT * FROM  `transaction` WHERE balance_number=:balanceNumber")
    fun getLastRecordForBalance(balanceNumber: Int) : List<Transaction>

}