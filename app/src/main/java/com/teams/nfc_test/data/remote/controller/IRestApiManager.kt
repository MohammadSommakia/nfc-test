package com.teams.nfc_test.data.remote.controller

import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse

internal interface IRestApiManager {

    suspend fun getExchangeRate(): Resource<ExchangeRateResponse>
}