package com.teams.nfc_test.presentation.write

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.teams.nfc_test.data.remote.controller.ErrorManager
import com.teams.nfc_test.data.remote.controller.Resource
import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import com.teams.nfc_test.data.repository.DataRepository
import com.teams.nfc_test.model.Transaction
import com.teams.nfc_test.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class WriteViewModel  @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {

    var exchangeRate = 1.2121
    val result = MutableLiveData<Event<List<Transaction>>>()
    val response = MutableLiveData<Event<Resource<ExchangeRateResponse>>>()

    fun getTheLastTransactionFor2Values()
    {
        // check the stored value in db
        launch {
            val transactionList = ArrayList<Transaction>()
            withContext(Dispatchers.IO) {
                transactionList.addAll(dataRepository.getLastRecordForBalance(1))
                transactionList.addAll(dataRepository.getLastRecordForBalance(2))
            }
            result.value =Event(transactionList)
        }
    }

    fun insertTransaction(transaction: Transaction) {
        launch {
            withContext(Dispatchers.IO) {
                dataRepository.insertTransaction(transaction)
            }
        }
    }

    fun getExchangeRateResponse()
    {
        launch {
            val exchangeRateResponse: Resource<ExchangeRateResponse> =
                dataRepository.getExchangeRate()


            when (exchangeRateResponse) {
                is Resource.Success -> {
                    exchangeRate =
                        (exchangeRateResponse.response as ExchangeRateResponse).rates.USD.toDouble()
                }

                is Resource.DataError -> {
                    val error = exchangeRateResponse.response as ErrorManager
                    response.value = Event(Resource.DataError(error))

                }
                is Resource.Exception -> {
                    val error = exchangeRateResponse.response as String
                    response.value = Event(Resource.Exception(error))

                }
                else -> {
                    //do nothing
                }
            }
        }
    }


}