package com.teams.nfc_test.utils;

/**
 * Only these types of Keys can be stored by the Helper class.
 */
public enum EnumKeyType {
    EnumAESKey,
    EnumDESKey,
    EnumMifareKey
}