package com.teams.nfc_test.di.module

import com.teams.nfc_test.presentation.read.ReadActivity
import com.teams.nfc_test.presentation.write.WriteActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {


    @ContributesAndroidInjector
    abstract fun contributeReadActivity(): ReadActivity

    @ContributesAndroidInjector
    abstract fun contributeWriteActivity(): WriteActivity
}