package com.teams.nfc_test.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "transaction")
class Transaction(


    @ColumnInfo(name = "balance_number")
    var balanceNumber: Int,

    @ColumnInfo(name = "old_balance_usd")
    var oldBalanceUSD: Int,

    @ColumnInfo(name = "new_balance_usd")
    var newBalanceUSD: Int,

    @ColumnInfo(name = "new_balance_eur")
    var newBalanceEUR: Double,

    @ColumnInfo(name = "exchange_rate")
    var exchangeRate: String

) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "transaction_id")
    var transactionId: Int = 1
}